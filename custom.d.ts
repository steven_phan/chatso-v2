declare module '*.svg' {
  const content: any
  export default content
}

// eslint-disable-next-line no-unused-vars
declare interface Window {
  gapi?: any
  map: any
  directionRenderer: any
  directionService: any
  durationMarker: any
  durationPosition: any
}

declare module '*.png' {
  export default '' as string
}
