import React from 'react'
import { Button } from '~/components/Elements/Button'
import { Button as ThemeButton } from '@mui/material'

export const Dashboard = () => {
  return (
    <div>
      <Button buttonType="primary">abc</Button>
      <ThemeButton color="primary">av</ThemeButton>
    </div>
  )
}
