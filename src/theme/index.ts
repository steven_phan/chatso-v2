import createTheme, {
  ThemeOptions as _ThemeOptions
} from '@mui/material/styles/createTheme'

interface ThemeOptions extends _ThemeOptions { }
// Augmenting Palette and Palette Options
declare module '@mui/material/styles/createPalette' {
  interface Palette {
    status: {
      success: string
      successDark: string
      warning: string
      warningDark: string
      error: string
      errorDark: string
    }
  }

  interface PaletteOptions {
    status?: {
      success?: string
      successDark?: string
      warning?: string
      warningDark?: string
      error?: string
      errorDark?: string
    }
  }
}

// Augmenting the Theme and ThemeOptions.
declare module '@mui/material/styles/createTheme' {
  interface Theme {
    name: string
    '@keyframes rotate': any
    '@keyframes dash': any
    bg: any
    color?: any
    status: any
  }

  // eslint-disable-next-line @typescript-eslint/no-shadow
  interface ThemeOptions {
    name?: string
    '@keyframes rotate'?: any
    '@keyframes dash'?: any
    bg?: any
    color?: any
    status: any
  }
}

export const colors = {
  primaryOrange: '#FA8C73',
  lightOrange: '#FA8C73',
  violet900: '#160E4D',
  violet600: '#BFC3E0',
  violet300: '#E9E7F6',
  lightViolet: '#F8F8FC',
  cantecOrange: '#FF7B5D',
  breadstackBlue: '#2834C5',
  red: '#F64066',
  lightRed: '#FFE6EC',
  green: '#73D6BA',
  lightGreen: '#EAFFF9',
  yellow: '#FFC149',
  lightYellow: '#FFF8ED',
  black: '#000000',
  gray900: '#797979',
  gray600: '#C9C9C9',
  gray300: '#F8F8F8',
  white: '#FFFFFF'
}

export const bgColors = {
  main: '#FCFCFF',
  white: '#fff',
  midPurple: '#E9E7F6',
  lightPurple: '#F8F8FC',
  darkPurple: '#160E4D',
  gray900: '#797979',
  gray600: '#C9C9C9',
  gray300: '#F8F8F8',
  green: '#73D6BA',
  lightGreen: '#EAFFF9',
  primaryOrange: '#FA8C73',
  lightOrange: '#FFF7F2',
  midOrange: '#FFE4D6',
  breadstack: '#2933C5',
  purpleOutline: '#BFC3E0',
  lightYellow: '#FFF8ED',
  red: '#F64066'
}

const themes: ThemeOptions = {
  shadows: [
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none',
    'none'
  ],
  spacing: 10,
  bg: bgColors,

  palette: {
    primary: {
      main: colors.primaryOrange,
      light: colors.lightOrange,
      dark: colors.black
    },
    text: {
      primary: colors.violet900
    },
    secondary: {
      main: colors.violet900,
      light: colors.lightViolet,
      dark: colors.black
    },
    status: {
      success: colors.lightGreen,
      successDark: colors.green,
      warning: colors.lightYellow,
      warningDark: colors.yellow,
      error: colors.lightRed,
      errorDark: colors.red
    }
  },
  typography: {
    fontFamily: ['DM Sans'].join(','),
    h1: {
      color: colors.violet900,
      fontWeight: 'bold',
      fontSize: 28
    },
    h2: {
      color: colors.violet900,
      fontWeight: 'bold',
      fontSize: 22
    },
    h3: {
      color: colors.violet900,
      fontWeight: 500,
      fontSize: 18
    },
    h4: {
      fontWeight: 'bold',
      fontSize: 16
    },
    subtitle1: {
      fontWeight: 'bold'
    },
    subtitle2: {
      fontWeight: 500
    },
    body1: {
      fontSize: 14
    },
    body2: {
      fontSize: 12
    }
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          fontSize: 14,
          lineHeight: 1,
          borderRadius: 18,
          minHeight: 35,
          backgroundColor: bgColors.midPurple,
          color: colors.violet900,
          '&.loading': {
            backgroundColor: `${bgColors.midPurple} !important`,
            color: `${colors.violet900} !important`,
            opacity: 0.4
          },
          '&$disabled': {
            color: colors.gray600,
            backgroundColor: bgColors.gray300
          },
          '&:hover': {
            backgroundColor: bgColors.midPurple
          }
        },
        containedPrimary: {
          backgroundColor: `${bgColors.primaryOrange}`,
          color: colors.white,
          fontWeight: 'normal',
          '&.loading': {
            backgroundColor: `${bgColors.primaryOrange} !important`,
            color: `${colors.white} !important`,
            opacity: 0.4
          },
          '&:hover': {
            backgroundColor: bgColors.primaryOrange
          },
          '&$disabled': {
            color: colors.gray600,
            backgroundColor: bgColors.gray300
          }
        },
        containedSecondary: {
          backgroundColor: bgColors.midOrange,
          color: colors.primaryOrange,
          fontWeight: 'normal',
          '&:hover, &:focus': {
            backgroundColor: bgColors.midOrange,
            color: colors.primaryOrange
          },
          '&.loading': {
            backgroundColor: `${bgColors.midOrange} !important`,
            color: `${colors.primaryOrange} !important`,
            opacity: 0.8
          },
          '&$disabled': {
            color: colors.gray600,
            backgroundColor: bgColors.gray300
          }
        }
      }
    }
  },
  color: colors,
  status: {},
  '@keyframes rotate': {
    from: {
      transform: 'rotate(0deg)'
    },
    to: {
      transform: 'rotate(360deg)'
    }
  },
  '@keyframes dash': {
    to: {
      'stroke-dashoffset': 0
    }
  }
}

export default createTheme(themes)
