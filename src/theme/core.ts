import {
  withStyles as _WithStyles,
  withTheme as _WithTheme
} from '@mui/material/styles'
import { SvgIconProps as _SVGIconProps } from '@mui/material/SvgIcon'
import { CSSProperties as _CSSProperties } from '@mui/styles'
import { Theme as _Theme } from '@mui/material/styles/createTheme'
type WS = typeof _WithStyles
type WT = typeof _WithTheme

export interface SvgIconProps extends _SVGIconProps { }

export interface WithStyles<P extends string> extends WS { }

export interface WithTheme extends WT { }

export interface CSSProperties extends _CSSProperties { }
export {
  createGenerateClassName,
  createStyles,
  jssPreset,
  ThemeProvider,
  makeStyles,
  withStyles,
  withTheme,
  useTheme
} from '@mui/styles'

export { createTheme } from '@mui/material/styles'

export interface Theme extends _Theme { }

export { default as useMediaQuery } from '@mui/material/useMediaQuery'
