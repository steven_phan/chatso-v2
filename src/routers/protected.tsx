import { Navigate, Outlet } from 'react-router-dom'
import { MainLayout } from '~/components/Layout/MainLayout'
import { lazyImport } from '~/utils/lazyImport'
const { Dashboard } = lazyImport(
  () => import('~/features/Dashboard'),
  'Dashboard'
)

const App = () => {
  return (
    <MainLayout>
      <Outlet />
    </MainLayout>
  )
}

export const protectedRoutes = [
  {
    path: '/',
    element: <App />,
    children: [
      { path: '/', element: <Dashboard /> },
      { path: '*', element: <Navigate to="." /> }
    ]
  }
]
