import { lazyImport } from '~/utils/lazyImport'
import { Navigate } from 'react-router-dom'

const { AuthRoutes } = lazyImport(() => import('~/features/Auth'), 'AuthRoutes')

export const publicRoutes = [
  {
    path: '/*',
    element: <AuthRoutes />
    // children: [{ path: '*', element: <Navigate to="/login" /> }]
  }
]
