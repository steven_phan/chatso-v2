import AppProvider from '~/providers/AppProvider'
import { AppRoutes } from './routers'

function App() {
  return (
    <AppProvider>
      <AppRoutes />
    </AppProvider>
  )
}

export default App
