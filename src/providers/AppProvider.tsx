import * as React from 'react'
import { ErrorBoundary } from 'react-error-boundary'
import { QueryClientProvider } from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'
import { BrowserRouter as Router } from 'react-router-dom'
import { queryClient } from '~/lib/react-query'
import ErrorBgImage from '~/assets/media/img/500.svg'
import { ThemeProvider } from '@mui/material/styles'
import theme from '~/theme'

const ErrorFallback = () => {
  return (
    <div>
      <h2>Ooops, something went wrong :( </h2>
      <img src={ErrorBgImage} width={500} height={500} alt="500" />
      <br />
      <button onClick={() => window.location.assign(window.location.origin)}>
        Refresh
      </button>
    </div>
  )
}

const AppProvider = ({ children }) => {
  return (
    <ThemeProvider theme={theme}>
      <React.Suspense
        fallback={
          <div className="h-screen w-screen flex items-center justify-center">
            ...Loading
          </div>
        }
      >
        <ErrorBoundary FallbackComponent={ErrorFallback}>
          <QueryClientProvider client={queryClient}>
            {process.env.NODE_ENV !== 'test' && <ReactQueryDevtools />}
            <Router>{children}</Router>
          </QueryClientProvider>
        </ErrorBoundary>
      </React.Suspense>
    </ThemeProvider>
  )
}

export default AppProvider
