import { Meta, Story } from '@storybook/react'

import { Button, ButtonProps } from './Button'

const meta: Meta = {
  title: 'Components/Elements/Button',
  component: Button,
  parameters: {
    controls: { expanded: true }
  }
}

export default meta

const Template: Story<ButtonProps> = props => <Button {...props} />

export const Primary = Template.bind({})
Primary.args = {
  children: 'Primary Button',
  buttonType: 'primary'
}
