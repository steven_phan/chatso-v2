import React from 'react'
import { ButtonProps as _ButtonProps } from '@mui/material/Button'
import { Button as ThemeButton } from '@mui/material'
import clsx from 'clsx'
import CircularProgress from '@mui/material/CircularProgress'
import { makeStyles, withStyles } from '~/theme/core'

export interface ButtonProps extends _ButtonProps {
  loading?: boolean
  destructive?: boolean
  buttonType?: 'primary' | 'secondary' | 'default' | 'text' | 'outlined'
  className?: string
  tooltipText?: string
  outline?: boolean
  deleteText?: string
  loadingText?: string
}

type CombinedProps = ButtonProps

const useStyles = makeStyles(() => ({
  root: {
    minWidth: 125,
    transition: 'none'
  }
}))

const CircularProgressStyled = withStyles(() => ({
  root: {
    marginLeft: 8
  }
}))(CircularProgress)

const getVariant = (buttonType: string): 'text' | 'outlined' | 'contained' => {
  switch (buttonType) {
    case 'primary':
    case 'secondary':
      return 'contained'
    case 'default':
    default:
      return undefined
  }
}

const getColor = (
  buttonType: string
):
  | 'inherit'
  | 'primary'
  | 'secondary'
  | 'success'
  | 'error'
  | 'info'
  | 'warning' => {
  switch (buttonType) {
    case 'primary':
      return 'primary'
    case 'secondary':
      return 'secondary'
    case 'default':
    default:
      return undefined
  }
}

export const Button: React.FC<CombinedProps> = props => {
  const classes = useStyles()

  const {
    loading,
    deleteText,
    tooltipText,
    loadingText,
    buttonType = 'primary',
    outline,
    className,
    children,
    disabled,
    ...rest
  } = props

  return (
    <>
      <ThemeButton
        {...rest}
        className={clsx(
          buttonType,
          {
            [classes.root]: true,
            loading
          },
          className
        )}
        disabled={disabled || loading}
        variant={getVariant(buttonType)}
        color={getColor(buttonType)}
      >
        {children}
        {loading && <CircularProgressStyled size={18} color="inherit" />}
      </ThemeButton>
    </>
  )
}
